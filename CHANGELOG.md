# Changelog

This is an auto-generated changelog file using the `modules/generate_changelog` script. We track the notable changes to this project using the conventional commit messages in the `main` branch.


## Versions



## Chores

- ceadc5a - chore: Update about section (Tue, 18 Jun 2024 21:11:53 +0800)
- 91990ad - chore: Enable comments in AE in TF2 yaml front matter (Wed, 15 May 2024 14:29:32 +0800)
- 5274317 - chore: Add updated Gemfile.lock (Tue, 14 May 2024 16:51:06 +0800)
- 2921e73 - chore: Remove Gemfile.lock (Tue, 14 May 2024 14:43:01 +0800)
- d462e88 - chore: Update about section (Tue, 14 May 2024 14:39:28 +0800)
- a9f4411 - chore: Update gemfile (Tue, 14 May 2024 14:39:11 +0800)
- b666e3a - chore: Add link to Springer publication (Sat, 11 Dec 2021 23:03:41 +0800)
- 63c3194 - chore: Remove CNAME (Fri, 5 Nov 2021 01:00:54 +0800)
- 9bbff6a - chore: add MLA citation for vanishing gradients post (Thu, 25 Feb 2021 13:10:57 +0800)
- c60de2e - chore: add text classification and clustering (Sat, 23 Jan 2021 14:28:22 +0800)
- 91d688c - chore: update email (Wed, 30 Dec 2020 07:11:15 +0800)
- 30e8ad7 - chore: use personal email (Tue, 10 Nov 2020 11:07:55 +0800)
- 74ccb23 - chore: revert to blank base url and url (Thu, 29 Oct 2020 14:05:31 +0800)
- 9dae41d - chore: add custom domain as profile url (Thu, 29 Oct 2020 14:00:34 +0800)
- ec642b0 - chore: add base url (Thu, 29 Oct 2020 13:57:08 +0800)
- 34d8952 - chore: add ae in pytorch post (Sun, 25 Oct 2020 18:53:31 +0800)
- 4ca35d3 - chore: add tag page for pytorch (Sun, 25 Oct 2020 18:53:13 +0800)
- 23f734c - chore: add excerpt (Fri, 23 Oct 2020 21:54:26 +0800)
- 6926364 - chore: add installing tf blog (Fri, 23 Oct 2020 21:51:41 +0800)
- ca3a0e5 - chore: update local branch (Fri, 23 Oct 2020 12:16:25 +0800)
- e5a4141 - chore: add separating line (Thu, 22 Oct 2020 21:49:58 +0800)
- 039207d - chore: add tag pages (Thu, 22 Oct 2020 18:56:55 +0800)
- b5e55d0 - chore: add tags page (Thu, 22 Oct 2020 18:56:42 +0800)
- cab8ed5 - chore: add tag collector (Thu, 22 Oct 2020 18:56:33 +0800)
- c7022d8 - chore: add tags (Thu, 22 Oct 2020 18:56:10 +0800)
- 77cb71a - chore: add tag generator script from long qian (Thu, 22 Oct 2020 18:55:38 +0800)
- 55e332e - chore: install rouge (Thu, 22 Oct 2020 18:37:13 +0800)
- c7f2546 - chore: add google analytics id (Thu, 22 Oct 2020 18:13:14 +0800)
- af7e759 - chore: install jekyll seo tag (Thu, 22 Oct 2020 17:43:24 +0800)
- 7076cb6 - chore: add share button styling (Thu, 22 Oct 2020 15:18:12 +0800)
- 6b9160e - chore: add share function (Thu, 22 Oct 2020 15:17:27 +0800)
- 16c91f5 - chore: add social icons (Thu, 22 Oct 2020 15:17:16 +0800)
- e973a8f - chore: add reading time calculator (Thu, 22 Oct 2020 14:53:52 +0800)
- 75779c5 - chore: remove vanishing gradients resources (Thu, 22 Oct 2020 14:31:32 +0800)
- 94383d3 - chore: add excerpts (Thu, 22 Oct 2020 01:31:55 +0800)
- 88b38aa - chore: add vanishing gradients post (Thu, 22 Oct 2020 01:22:25 +0800)
- dc8792b - chore: add assets for vanishing gradients post (Thu, 22 Oct 2020 01:22:16 +0800)
- b99d2df - chore: add google site verification (Wed, 21 Oct 2020 21:12:44 +0800)
- 8f0cc55 - chore: add jekyll-sitemap (Wed, 21 Oct 2020 21:12:26 +0800)
- be9de86 - chore: enable google analytics (Wed, 21 Oct 2020 19:16:03 +0800)
- 9814ee7 - chore: add stackoverflow profile link (Wed, 21 Oct 2020 19:14:24 +0800)
- d4e1232 - chore: use production environment (Wed, 21 Oct 2020 19:09:32 +0800)
- f6620aa - chore: uncomment header (Wed, 21 Oct 2020 18:45:32 +0800)
- 790e656 - chore: add site url (Wed, 21 Oct 2020 18:45:21 +0800)
- 010e841 - chore: remove welcome to jekyll post (Wed, 21 Oct 2020 13:28:26 +0800)
- b472b34 - chore: add tf-ae blog post (Wed, 21 Oct 2020 13:16:13 +0800)
- a760b11 - chore: add images for tf-ae post (Wed, 21 Oct 2020 13:16:04 +0800)
- ee6a1fe - chore: add site metadata (Wed, 21 Oct 2020 12:48:46 +0800)
- 8af5297 - chore: re-add about (Wed, 21 Oct 2020 12:45:06 +0800)
- 3192b3a - chore: use minima from github (Wed, 21 Oct 2020 12:42:59 +0800)
- c07c47a - chore: modify config (Wed, 21 Oct 2020 12:36:37 +0800)
- 75963be - chore: remove _site (Wed, 21 Oct 2020 12:31:33 +0800)
- 0c00e78 - chore: add site (Wed, 21 Oct 2020 12:30:09 +0800)
- 11167e4 - chore: populate about.md (Wed, 21 Oct 2020 12:28:36 +0800)
- 2f0e9b9 - chore: use jekyll minima (Wed, 21 Oct 2020 12:28:20 +0800)
- 03c6c68 - chore: remove academic pages template (Wed, 21 Oct 2020 12:20:53 +0800)
- b352db3 - chore: remove teaching from navbar (Sat, 1 Aug 2020 16:35:09 +0800)
- 26b5e7a - chore: remove teaching page (Sat, 1 Aug 2020 16:31:12 +0800)
- d78eff4 - chore: remove templates (Sat, 1 Aug 2020 16:26:45 +0800)
- 8c17d63 - chore: remove template blogs (Sat, 1 Aug 2020 16:25:27 +0800)
- 9699628 - chore: add blog post (Sun, 26 Jul 2020 21:50:00 +0800)
- 8b70ddf - chore: add so profile (Sun, 26 Jul 2020 21:49:23 +0800)
- ae16c94 - chore: add ae tf2 post (Sun, 26 Jul 2020 21:49:04 +0800)
- 823e4ab - chore: add list of publications (Mon, 13 Jul 2020 16:38:57 +0800)
- 61d57b8 - chore: add clustering + ae publication (Mon, 13 Jul 2020 16:38:26 +0800)
- 47c8414 - chore: add dist sys publication (Mon, 13 Jul 2020 16:30:02 +0800)
- 211ceed - chore: add ialp publication (Mon, 13 Jul 2020 16:23:14 +0800)
- 51834fd - chore: add ecommerce reviews analysis publication (Mon, 13 Jul 2020 16:15:01 +0800)
- cbfe341 - chore: add dl-svm for malware classification publication (Mon, 13 Jul 2020 16:11:11 +0800)
- 93856b0 - chore: add cnn-svm publication (Sun, 12 Jul 2020 22:51:11 +0800)
- a8a0374 - chore: add wdbc publication (Sun, 12 Jul 2020 22:47:35 +0800)
- 7d017cd - chore: add gru-svm publication (Sun, 12 Jul 2020 22:09:49 +0800)
- 1822a9a - chore: remove template markdown files for publications (Sun, 12 Jul 2020 22:09:37 +0800)
- dbd809d - chore: add projects section in about (Sun, 12 Jul 2020 17:20:27 +0800)
- 7c2a9dc - chore: remove link to ja (Thu, 9 Jul 2020 19:21:48 +0800)
- 08950f4 - chore: configure site (Thu, 9 Jul 2020 19:16:21 +0800)
- 0bcdf8b - chore: add starting content for about me (Thu, 9 Jul 2020 19:16:05 +0800)
- aff7352 - chore: set yaml configuration (Thu, 9 Jul 2020 17:58:07 +0800)
- cbf19b3 - chore: add page template (Wed, 8 Jul 2020 16:59:34 +0800)
- b26ab55 - chore: add profile picture (Wed, 8 Jul 2020 16:58:23 +0800)
- 25d2a4e - chore: add test configuration (Wed, 8 Jul 2020 16:58:07 +0800)
- 240d99d - chore: add template from academicpages/academicpages.github.io (Wed, 8 Jul 2020 16:11:29 +0800)


## Documentation

- 72781ee - docs: Update work experience (Sun, 21 Jul 2024 21:39:59 +0800)
- 6280d7d - docs: Update about section (Sun, 2 Jun 2024 13:23:54 +0800)
- 3c4cd4b - docs: Add acceptance of IJCNN 2022 paper (Sun, 8 May 2022 22:24:51 +0800)
- 1528400 - docs: Add link to IEEEXplore (Thu, 24 Mar 2022 13:44:14 +0800)
- 1f33e8b - docs: Update description (Thu, 24 Mar 2022 13:34:27 +0800)
- 01fd8c9 - docs: Update about page (Thu, 24 Mar 2022 13:31:41 +0800)
- 590b0d4 - docs: Add ICONIP paper (Fri, 5 Nov 2021 04:36:46 +0800)
- c84745e - docs: add tf version compatible with GTX960M (Thu, 31 Dec 2020 16:12:02 +0800)
- 8cacf32 - docs: rectify program (Mon, 28 Dec 2020 12:10:51 +0800)
- 85546f4 - docs: add original publication links (Fri, 30 Oct 2020 15:37:21 +0800)
- 5090f48 - docs: add citation (Mon, 26 Oct 2020 17:55:25 +0800)
- 35ac22f - docs: add recommended citation (Thu, 22 Oct 2020 14:27:50 +0800)
- e3d0f1c - docs: fix excerpt for vanishing gradients post (Thu, 22 Oct 2020 01:47:18 +0800)
- d2312f6 - docs: add recommended citation (Wed, 21 Oct 2020 18:32:28 +0800)
- 1740e92 - docs: update description (Wed, 21 Oct 2020 12:54:26 +0800)
- 623712b - docs: remove year due to redundancy (Mon, 13 Jul 2020 16:23:51 +0800)
- 67da6b5 - docs: add tldr indicator (Sun, 12 Jul 2020 22:58:17 +0800)
- 47c1bbd - docs: add list of projects in about page (Sun, 12 Jul 2020 17:18:43 +0800)
- a807403 - docs: update about page (Fri, 10 Jul 2020 20:12:37 +0800)
- 6d50f27 - docs: add work experience section (Fri, 10 Jul 2020 20:12:13 +0800)
- 577a8ec - docs: edit intro (Fri, 10 Jul 2020 19:42:35 +0800)
- 641b356 - docs: add latest publication section (Fri, 10 Jul 2020 19:42:14 +0800)
- c9d5303 - docs: remove ja (Fri, 10 Jul 2020 19:27:20 +0800)


## Features

- b65dca3 - feat: display tags (Thu, 22 Oct 2020 18:57:12 +0800)
- 0126ef3 - feat: add share buttons (Thu, 22 Oct 2020 15:18:29 +0800)
- a37c638 - feat: add reading time (Thu, 22 Oct 2020 14:54:06 +0800)
- 9a2937c - feat: enable showing of excerpts (Thu, 22 Oct 2020 01:32:07 +0800)
- faef3df - feat: enable disqus (Wed, 21 Oct 2020 18:39:18 +0800)
- 5167fa7 - feat: enable automatic numbering in mathjax (Wed, 21 Oct 2020 13:10:33 +0800)
- 25d02e9 - feat: enable mathjax (Wed, 21 Oct 2020 13:04:50 +0800)


## Fixes

- ac8efd8 - fix: Use updated disqus shortname (Tue, 14 May 2024 17:10:46 +0800)
- 56db3f0 - fix: Rectify url and baseurl (Tue, 14 May 2024 17:05:14 +0800)
- a902fc5 - fix: Run jekyll build on public dir (Tue, 14 May 2024 16:32:42 +0800)
- 6e8e447 - fix: Rectify typo on USA (Sun, 8 May 2022 22:23:10 +0800)
- cfd32d4 - fix: Rectify typo on Yu-Han's name (Thu, 24 Mar 2022 13:42:18 +0800)
- 443294d - fix: remove profile url (Thu, 29 Oct 2020 13:58:18 +0800)
- 9c65b1e - fix: rectify citation entry (Mon, 26 Oct 2020 17:56:08 +0800)
- 376b145 - fix: rectify link to figure 1 (Sun, 25 Oct 2020 19:02:23 +0800)
- bd0a7b4 - fix: use semicolon (Thu, 22 Oct 2020 17:49:32 +0800)
- c1d8a93 - fix: move share button styles in its own scss (Thu, 22 Oct 2020 15:25:30 +0800)
- 5c33f16 - fix: rectify site url (Wed, 21 Oct 2020 20:36:55 +0800)
- 84a4d7b - fix: add correct disqus shortname (Wed, 21 Oct 2020 20:19:58 +0800)
- 02d64d9 - fix: add correct disqus shortname (Wed, 21 Oct 2020 20:16:34 +0800)
- 2e3dabd - fix: rectify so id (Wed, 21 Oct 2020 19:17:50 +0800)
- de2f335 - fix: add missing images (Sun, 2 Aug 2020 21:10:07 +0800)
- 1ebef86 - fix: correct typo (Fri, 24 Jul 2020 19:04:01 +0800)
- abe2a41 - fix: remove redundant about me section (Thu, 9 Jul 2020 19:19:31 +0800)
- acf94ee - fix: add site url (Wed, 8 Jul 2020 17:01:29 +0800)


## Improvements



## Refactors

- e5615db - refactor: add equation links, add github gist (Sun, 25 Oct 2020 19:00:12 +0800)
- b8e4039 - refactor: add newline before authors (Fri, 23 Oct 2020 12:16:07 +0800)
- ead75bb - refactor: remove commented socials (Thu, 22 Oct 2020 17:51:14 +0800)
- df16ea2 - refactor: add original publication notice (Thu, 22 Oct 2020 14:29:14 +0800)
- 1c2e8b0 - refactor: add links and reformat tables (Thu, 22 Oct 2020 14:24:57 +0800)
- 01760ef - refactor: reformat tf-ae post (Wed, 21 Oct 2020 18:28:03 +0800)


## Tests

